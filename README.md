# NetworkLayer

This is a Network Layer responsible for making API calls. It handles everything from URLEncoding and JSONEncoding to response decoding and error handling.

To use it for API calls, do the following:
- Create an enum that conforms to `EndPointType`.
- Create a class that conforms to `SessionManager` if you would like to customize the `URLSession`, conform to `URLSessionDownloadDelegate`, etc.
- Create a Router instance with the enum comforming to `EndPointType`. 
- Call its `.request` function with the corresponding parameters.

To use it for image loading calls, do the following:
- Create a `URLRequest`.
- Create a class that conforms to `SessionManager` if you would like to customize the `URLSession`, conform to `URLSessionDownloadDelegate`, etc.
- Create a class that conforms to `ImageLoader` if you would like to customize the `URLCache`.
- Create an instance of `DefaultImageLoader`.
- call its `.request` function with the corresponding parameters.

For a more detailed implementation example, you can check the `NetworkLayerTests.swift` file.
