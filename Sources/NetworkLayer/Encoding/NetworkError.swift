//
//  NetworkError.swift
//  NetworkLayer
//
//  Created by ElioHanna on 1/20/20.
//  Copyright © 2020 ElioHanna. All rights reserved.
//

import Foundation

public enum NetworkRequestError : String, Error {
    case parametersNil = "Parameters were nil."
    case encodingFailed = "Parameter encoding failed."
    case missingURL = "URL is nil."
}

public enum NetworkResponseError: String, Error {
	case noData = "Response returned with no data to decode."
    case authenticationError = "You need to be authenticated first."
    case badRequest = "Bad request"
    case outdated = "The url you requested is outdated."
    case failed = "Network request failed."
    case unableToDecode = "We could not decode the response."
}
