//
//  ParameterEncoder.swift
//  NetworkLayer
//
//  Created by ElioHanna on 1/22/20.
//  Copyright © 2020 ElioHanna. All rights reserved.
//

import Foundation

protocol ParameterEncoder {
	static func encode<T: Codable>(urlRequest: inout URLRequest, parameters: T) throws
}
