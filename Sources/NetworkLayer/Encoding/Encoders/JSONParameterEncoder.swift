//
//  JSONParameterEncoder.swift
//  NetworkLayer
//
//  Created by ElioHanna on 1/22/20.
//  Copyright © 2020 ElioHanna. All rights reserved.
//

import Foundation

struct JSONParameterEncoder: ParameterEncoder {
	static func encode<T: Codable>(urlRequest: inout URLRequest, parameters: T) throws {
		do {
			let jsonAsData = try JSONEncoder().encode(parameters)
			urlRequest.httpBody = jsonAsData
			if urlRequest.value(forHTTPHeaderField: "Content-Type") == nil {
				urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
			}
		}
		catch {
			throw NetworkRequestError.encodingFailed
		}
	}
}
