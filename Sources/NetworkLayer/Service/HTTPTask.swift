//
//  HTTPTask.swift
//  NetworkLayer
//
//  Created by ElioHanna on 1/20/20.
//  Copyright © 2020 ElioHanna. All rights reserved.
//

import Foundation

public typealias HTTPHeaders = [String: String]

public enum HTTPTask<BodyParameter: Codable, URLParameter: Codable> {
	case request
	case requestParameters(bodyParameters: BodyParameter?, urlParameters: URLParameter?)
	case requestParametersAndHeaders(bodyParameters: BodyParameter?, urlParameters: URLParameter?, headers: HTTPHeaders?)
}
