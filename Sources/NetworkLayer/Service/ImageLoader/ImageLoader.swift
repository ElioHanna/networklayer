//
//  ImageLoader.swift
//  NetworkLayer
//
//  Created by ElioHanna on 1/22/20.
//  Copyright © 2020 ElioHanna. All rights reserved.
//

import Foundation

public protocol ImageLoader: class {
	var manager: SessionManager { get }
	var cache: URLCache { get }
	
	func request(_ urlRequest: URLRequest, forceRefresh: Bool, completion: @escaping (Result<Data, Error>) -> Void)
	func cancel(_ urlRequest: URLRequest)
}
