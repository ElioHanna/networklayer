//
//  NetworkRouter.swift
//  NetworkLayer
//
//  Created by ElioHanna on 1/22/20.
//  Copyright © 2020 ElioHanna. All rights reserved.
//

import Foundation

public protocol NetworkRouter: class {
	associatedtype EndPoint: EndPointType
	
	func request<T: Codable>(_ route: EndPoint, completion: @escaping (Result<T?, Error>) -> Void)
	func cancel(_ route: EndPoint)
}
