//
//  HTTPMethod.swift
//  NetworkLayer
//
//  Created by ElioHanna on 1/20/20.
//  Copyright © 2020 ElioHanna. All rights reserved.
//

import Foundation

public enum HTTPMethod: String {
	case get = "GET"
	case post = "POST"
	case put = "PUT"
	case delete = "DELETE"
}
