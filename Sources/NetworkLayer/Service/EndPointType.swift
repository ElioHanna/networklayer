//
//  EndPointType.swift
//  NetworkLayer
//
//  Created by ElioHanna on 1/20/20.
//  Copyright © 2020 ElioHanna. All rights reserved.
//

import Foundation

public protocol EndPointType {
	associatedtype BodyParameters: Codable
	associatedtype URLParameters: Codable
	
	var baseURL: URL { get }
    var path: String { get }
    var httpMethod: HTTPMethod { get }
    var task: HTTPTask<BodyParameters, URLParameters> { get }
    var headers: HTTPHeaders? { get }
}
