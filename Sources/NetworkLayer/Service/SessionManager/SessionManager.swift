//
//  SessionManager.swift
//  NetworkLayer
//
//  Created by ElioHanna on 1/20/20.
//  Copyright © 2020 ElioHanna. All rights reserved.
//

import Foundation

public protocol SessionManager: class {
	var session: URLSession { get }
	var tasks: [URLSessionTask] { get }
	
	func request(_ urlRequest: URLRequest, completion: @escaping (Result<(Data?, URLResponse?), Error>) -> Void)
	func cancel(_ urlRequest: URLRequest)
}
