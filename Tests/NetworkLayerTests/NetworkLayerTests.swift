import XCTest
@testable import NetworkLayer

final class NetworkLayerTests: XCTestCase {
	enum JSONPlaceholder: EndPointType {
		typealias BodyParameters = Post
		typealias URLParameters = [String: String]
		
		case getResource(id: Int)
		case addResource(_ resource: BodyParameters)
		case updateResource(_ resource: BodyParameters)
		case deleteResource(id: Int)
		case filterResources(predicate: URLParameters)
		
		var baseURL: URL {
			let url = URL(string: "https://jsonplaceholder.typicode.com/")
			XCTAssertNotNil(url)
			return url!
		}
		
		var path: String {
			switch self {
			case .getResource(let id):
				return "posts/\(id)"
			case .addResource(_):
				return "posts"
			case .updateResource(let resource):
				guard let id = resource.id else { return "posts" }
				return "posts/\(id)"
			case .deleteResource(let id):
				return "posts/\(id)"
			case .filterResources(_):
				return "posts"
			}
		}
		
		var httpMethod: HTTPMethod {
			switch self {
			case .getResource(_):
				return .get
			case .addResource(_):
				return .post
			case .updateResource(_):
				return .put
			case .deleteResource(_):
				return .delete
			case .filterResources(_):
				return .get
			}
		}
		
		var task: HTTPTask<BodyParameters, URLParameters> {
			switch self {
			case .getResource(_):
				return .request
			case .addResource(let resource):
				return .requestParameters(bodyParameters: resource, urlParameters: nil)
			case .updateResource(let resource):
				return .requestParameters(bodyParameters: resource, urlParameters: nil)
			case .deleteResource(_):
				return .requestParameters(bodyParameters: nil, urlParameters: nil)
			case .filterResources(let predicate):
				return .requestParameters(bodyParameters: nil, urlParameters: predicate)
			}
		}
		
		var headers: HTTPHeaders? {
			return nil
		}
	}
	
	struct Post: Codable, Equatable {
		let id: Int?
		let title: String?
		let body: String?
		let userId: Int?
		
		init(id: Int? = nil, title: String? = nil, body: String? = nil, userId: Int? = nil) {
			self.id = id
			self.title = title
			self.body = body
			self.userId = userId
		}
	}
	
	func testGet() {
		let getExpectation = expectation(description: "Testing GET")
		
		let id = 1
		
		Router<JSONPlaceholder>().request(.getResource(id: id), completion: { (response: Result<Post?, Error>) in
			switch response {
			case.success(let result):
				guard result?.id == 1 else { return }
				getExpectation.fulfill()
			case .failure(let error):
				XCTAssert(false, error.localizedDescription)
			}
		})
		
		waitForExpectations(timeout: 60, handler: nil)
	}
	
	func testPost() {
		let postExpectation = expectation(description: "Testing POST")
		
		let expectedResponse = Post(id: 101,
									title: "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",
									body: """
	quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto
	""",
									userId: 1)
		
		Router<JSONPlaceholder>().request(.addResource(expectedResponse), completion: { (response: Result<Post?, Error>) in
			switch response {
			case.success(let result):
				guard result == expectedResponse else { return }
				postExpectation.fulfill()
			case .failure(let error):
				XCTAssert(false, error.localizedDescription)
			}
		})
		
		waitForExpectations(timeout: 60, handler: nil)
	}
	
	func testPut() {
		let putExpectation = expectation(description: "Testing PUT")
		
		let expectedResponse = Post(id: 1,
									title: "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",
									body: """
	quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto
	""",
									userId: 1)
		
		Router<JSONPlaceholder>().request(.updateResource(expectedResponse), completion: { (response: Result<Post?, Error>) in
			switch response {
			case.success(let result):
				guard result == expectedResponse else { return }
				putExpectation.fulfill()
			case .failure(let error):
				XCTAssert(false, error.localizedDescription)
			}
		})
		
		waitForExpectations(timeout: 60, handler: nil)
	}
	
	func testDelete() {
		let deleteExpectation = expectation(description: "Testing DELETE")
		
		Router<JSONPlaceholder>().request(.deleteResource(id: 1), completion: { (response: Result<Post?, Error>) in
			switch response {
			case.success(_):
				deleteExpectation.fulfill()
			case .failure(let error):
				XCTAssert(false, error.localizedDescription)
			}
		})
		
		waitForExpectations(timeout: 60, handler: nil)
	}
	
	func testGetWithParameters() {
		let getExpectation = expectation(description: "Testing GET with parameters")
		
		let id = 1
		let params = ["userId": "\(id)"]
		
		Router<JSONPlaceholder>().request(.filterResources(predicate: params), completion: { (response: Result<[Post]?, Error>) in
			switch response {
			case.success(let result):
				let allIds = result?.compactMap({ $0.userId })
				if allIds?.allSatisfy({ $0 == id }) ?? false {
					getExpectation.fulfill()
				}
			case .failure(let error):
				XCTAssert(false, error.localizedDescription)
			}
		})
		
		waitForExpectations(timeout: 60, handler: nil)
	}
	
	func testImageLoading() {
		let loadingExpectation = expectation(description: "Testing loading an image")
		
		guard let imageURL = URL(string: "https://www.iclarified.com/images/news/72562/72562/72562-1280.jpg") else {
			XCTAssert(false, "Could not create URL.")
			return
		}
		let urlRequest = URLRequest(url: imageURL)
		
		DefaultImageLoader().request(urlRequest, completion: { response in
			switch response {
			case .success(_):
				loadingExpectation.fulfill()
			case .failure(let error):
				XCTAssert(false, error.localizedDescription)
			}
		})
		
		waitForExpectations(timeout: 60, handler: nil)
	}
	
	static var allTests = [
		("testGet", testGet),
		("testPost", testPost),
		("testPut", testPut),
		("testDelete", testDelete),
		("testGetWithParameters", testGetWithParameters),
		("testImageLoading", testImageLoading)
	]
}
